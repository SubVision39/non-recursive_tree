﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Non_recursiveTree
{
    public class Tree<T> : IEnumerable<TreeNode<T>>
    {
        public TreeNode<T> Root { get; }

        public Tree(T initialValue = default(T))
        {
            Root = new TreeNode<T>(initialValue);
        }
        public Tree(TreeNode<T> root)
        {
            Root = root;
        }

        public void Add(T value)
        {
            Root.Value = value;
        }
        public void Add(TreeNode<T> node)
        {
            Root.Nodes.Add(node);
        }
        public IEnumerator<TreeNode<T>> GetEnumerator()
        {
            return Root.Nodes.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        public void ForEachVertical(Action<TreeNode<T>> a)
        {
            Stack<TreeNode<T>> stack = new Stack<TreeNode<T>>();
            stack.Push(Root);
            while (stack.Count > 0)
            {
                TreeNode<T> node = stack.Pop();
                a(node);
                foreach (var subNode in node.Nodes.Reverse<TreeNode<T>>())
                    stack.Push(subNode);
            }
        }
        public void ForEachHorisontal(Action<TreeNode<T>> a)
        {
            Queue<TreeNode<T>> stack = new Queue<TreeNode<T>>();
            stack.Enqueue(Root);
            while (stack.Count > 0)
            {
                TreeNode<T> node = stack.Dequeue();
                a(node);
                foreach (var subNode in node.Nodes)
                    stack.Enqueue(subNode);
            }
        }

    }
}
