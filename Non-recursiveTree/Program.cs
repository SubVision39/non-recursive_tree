﻿using System;

namespace Non_recursiveTree
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Vertical");
            Tree<int> tree = new Tree<int> (1)
            {
                  new TreeNode<int>(2)
                  {
                        new TreeNode<int> (3),
                        new TreeNode<int> (4)
                        {
                            new TreeNode<int>(5)
                        },
                        new TreeNode<int> (6)
                  },
                  new TreeNode<int>(7){ },
                };
            tree.ForEachVertical(n => Console.WriteLine(n.Value));
            Console.WriteLine("Horisontal");
            tree = new Tree<int>(1)
            {
                  new TreeNode<int>(2)
                  {
                        new TreeNode<int> (4),
                        new TreeNode<int> (5)
                        {
                            new TreeNode<int>(7)
                        },
                        new TreeNode<int> (6)
                  },
                  new TreeNode<int>(3){ },
                };
            tree.ForEachHorisontal(n => Console.WriteLine(n.Value));
        }
    }
}
