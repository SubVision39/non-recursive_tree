﻿using System.Collections;
using System.Collections.Generic;

namespace Non_recursiveTree
{
    public class TreeNode<T> : IEnumerable<TreeNode<T>>
    {
        public T Value { get; set; }
        public List<TreeNode<T>> Nodes { get; }

        public TreeNode(T initialValue = default(T))
        {
            Value = initialValue;
            Nodes = new List<TreeNode<T>>();
        }
        public void Add(T value)
        {
            Value = value;
        }
        public void Add(TreeNode<T> node)
        {
            Nodes.Add(node);
        }
        public IEnumerator<TreeNode<T>> GetEnumerator()
        {
            return Nodes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}